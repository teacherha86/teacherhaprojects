using UnityEngine;

namespace ProfilingDemo
{
    public class Demo : MonoBehaviour
    {
        public Material mat;
        public GameObject prefab;

        [ContextMenu("make")]
        void MakeSphere()
        {
            GameObject root = new GameObject("root");
            for(int i = 0; i < 10000; i++)
            {
                var pos = Random.insideUnitCircle;
                Object.Instantiate(prefab, pos, Quaternion.identity, root.transform);
            //    go.GetComponent<MeshRenderer>().material.color = Color.red;
            }
          //  CombineMesh(root);
        }

        void CombineMesh(GameObject root)
        {
            {
                
                MeshFilter[] meshFilters = root.GetComponentsInChildren<MeshFilter>();
                CombineInstance[] combine = new CombineInstance[meshFilters.Length];
                int i = 0;
                while (i < meshFilters.Length)
                {
                    combine[i].mesh = meshFilters[i].sharedMesh;
                    combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                    meshFilters[i].gameObject.SetActive(false);

                    i++;
                }

                Mesh mesh = new Mesh();
                mesh.CombineMeshes(combine);
                var go = new GameObject("CombinedMesh");
                go.AddComponent<MeshFilter>().sharedMesh = mesh;
                go.AddComponent<MeshRenderer>().sharedMaterial = mat;
            }
        }

        void MyFunction ()
        {
            // it takes long times to finish
            for (int i = 0; i < 1000; i++)
            {
                // Debug.Log(i.ToString());
            }
        }

        private void Start()
        {
            for(int i = 0; i < 10; i++)
                MakeSphere();
        }


        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A)) 
            {
                MyFunction();
            }        
        }
    }
}
