using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;


namespace FirebaseDemo
{
    public class Demo : MonoBehaviour
    {
        Firebase.FirebaseApp app;
        Firebase.Auth.FirebaseAuth auth;
        FirebaseFirestore db;

        private void OnDestroy()
        {
            auth.Dispose();

        }

        // Start is called before the first frame update
        void Start()
        {            
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    app = Firebase.FirebaseApp.DefaultInstance;
                    auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                    

                    db = FirebaseFirestore.DefaultInstance;

                    // caching.....

                    ReadData();
                  //  SignUpNewUser("hhh@example.com", "123456");
                  //  GuestLogin();
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }

        void GuestLogin ()
        {
            auth.SignInAnonymouslyAsync().ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("SignInAnonymouslyAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.AuthResult result = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    result.User.DisplayName, result.User.UserId);
            });
        }

        void SignUpNewUser (string email, string password)
        {
            auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    SignIn(email, password);                    
                    return;
                }

                // Firebase user has been created.
                Firebase.Auth.AuthResult result = task.Result;
                Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                  result.User.DisplayName, result.User.UserId);
            });
        }


        void SignIn(string email, string password)
        {
            auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.AuthResult result = task.Result;                
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                  result.User.DisplayName, result.User.UserId);
                AddData();
                //result.User.TokenAsync(false).ContinueWith(token =>
                //{
                //    Debug.Log(token.Result);

                //    AddData();
                //});
            });
        }


        void AddData()
        {            
            DocumentReference docRef = db.Collection("users").Document(auth.CurrentUser.UserId);
            Dictionary<string, object> user = new Dictionary<string, object>{
                { "name", "test name" },
                { "addres", "hell1" },
            };
            docRef.SetAsync(user).ContinueWithOnMainThread(task => {
                Debug.Log("Added data to the alovelace document in the users collection.");
                ReadData();
            });
        }

        void ReadData()
        {
            var usersRef = db.Collection("users").Document("bank");
            usersRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
            {
                DocumentSnapshot snapshot = task.Result;
                if (snapshot.Exists)
                {
                    Debug.Log(System.String.Format("Document data for {0} document:", snapshot.Id));
                    Dictionary<string, object> city = snapshot.ToDictionary();
                    foreach (KeyValuePair<string, object> pair in city)
                    {
                        Debug.Log(System.String.Format("{0}: {1}", pair.Key, pair.Value));
                    }
                }
                else
                {
                    Debug.Log(System.String.Format("Document {0} does not exist!", snapshot.Id));
                }

            });
                //QuerySnapshot snapshot = task.Result;
                //foreach (DocumentSnapshot document in snapshot.Documents)
                //{


                    //Debug.Log(String.Format("User: {0}", document.Id));
                    //Dictionary<string, object> documentDictionary = document.ToDictionary();
                    //Debug.Log(String.Format("First: {0}", documentDictionary["First"]));
                    //if (documentDictionary.ContainsKey("Middle"))
                    //{
                    //    Debug.Log(String.Format("Middle: {0}", documentDictionary["Middle"]));
                    //}

                    //Debug.Log(String.Format("Last: {0}", documentDictionary["Last"]));
                    //Debug.Log(String.Format("Born: {0}", documentDictionary["Born"]));
            //    }

            //    Debug.Log("Read all data from the users collection.");
            //});
        }

    }
}
