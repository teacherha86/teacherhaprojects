using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace DataStructuresDemo
{
    public class QueueDemo : MonoBehaviour
    {
        Queue<GameObject> bullets = new Queue<GameObject>();      
        

        public GameObject bulletPrefab = null;

        GameObject MakeBullet ()
        {
            var go = Object.Instantiate(bulletPrefab);
            go.SetActive(false);
            return go;
        }

        private void Start()
        {
            for(int i = 0; i < 20; i++)
            {
                bullets.Enqueue(MakeBullet());
            }
        }

        void Shoot()
        {
            var bullet = bullets.Dequeue();
            bullet.SetActive(true);
            Pooling();
            async void Pooling ()
            {
                await Task.Delay(1000);
                bullet.SetActive(false);
                bullets.Enqueue(bullet);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                Shoot();
            }
        }
    }
}
