using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DataStructuresDemo
{
    public class StackPopup : MonoBehaviour
    {
        public GameObject uiPrefab;
        public Transform anchor;

        Stack<GameObject> UIs = new Stack<GameObject>();


        GameObject CurrentUI => 0 < UIs.Count ? UIs.Peek() : null;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
                ShowUI();
            if (Input.GetKeyDown(KeyCode.S))
                HideUI();
        }

        void ShowUI()
        {
            if(CurrentUI != null)
            {
                CurrentUI.SetActive(false);
            }
            var go = GameObject.Instantiate(uiPrefab, anchor);
            go.SetActive(true);
            UIs.Push(go);             
            go.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            go.transform.GetComponent<Text>().text = UIs.Count.ToString();
        }

        void HideUI()
        {
            if(0 < UIs.Count)
            {
                Object.Destroy(UIs.Pop());
                if (CurrentUI != null)
                {
                    CurrentUI.SetActive(true);
                }
            }
        }

    }
}
