using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;


namespace DataStructuresDemo
{
    public class Main : MonoBehaviour
    {
        Stack<int> one = new Stack<int>();
        Stack<int> two = new Stack<int>();
        Stack<int> three = new Stack<int>();


        Stack<int> stack = new Stack<int>();
        // Start is called before the first frame update
        void Start()
        {
            one.Push(3);
            one.Push(2);
            one.Push(1);

            three.Push(one.Pop());
            two.Push(one.Pop());

            two.Push(three.Pop());

            three.Push(one.Pop());
            one.Push(two.Pop());
            three.Push(two.Pop());
            three.Push(one.Pop());



            while(0 < three.Count)
            {
                Debug.Log(three.Pop());
            }




            stack.Push(0);
            stack.Push(1);


            Call();

            Call2();

            stack.Pop();
        }


        void Call()
        {
            stack.Push(2);
            Call2();
        }
        void Call2()
        {
            stack.Push(3);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
