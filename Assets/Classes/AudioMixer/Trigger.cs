using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioMixerDemo
{
    public class Trigger : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            GameObject.FindObjectOfType<Demo>().InRoom();
        }

        private void OnTriggerExit(Collider other)
        {
            GameObject.FindObjectOfType<Demo>().Open();
        }
    }
}