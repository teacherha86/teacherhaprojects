using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace AudioMixerDemo
{
    public class Demo : MonoBehaviour
    {
        public AudioMixer m_AudioMixer;

        public float transitionTimes = 1.0f;
        public AudioMixerSnapshot m_Open;
        public AudioMixerSnapshot m_Close;
        public AudioMixerSnapshot m_BGOnly;
        public AudioMixerSnapshot m_InRoom;


        public void Open()
        {
            m_Open.TransitionTo(transitionTimes);
        }

        public void Close()
        {
            m_Close.TransitionTo(transitionTimes);
        }

        public void BGOnly ()
        {
            m_BGOnly.TransitionTo(transitionTimes);
        }

        [ContextMenu("InRoom")]
        public void InRoom()
        {
            m_InRoom.TransitionTo(transitionTimes);
        }

        public void UpdateMasterVolume(float volume)
        {
            m_AudioMixer.SetFloat("MasterVolume", volume);
        }

        public void UpdateBGVolume(float volume)
        {
            m_AudioMixer.SetFloat("BGVolume", volume);
        }

    }
}
